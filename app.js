const express = require('express');
const app = express();
const mongoose = require('mongoose');

require('dotenv/config');

app.use(express.json());
app.use(express.urlencoded({extended: true}));


//Import Routes
const postRoute = require('./routes/posts');

app.use("/posts", postRoute);


// Routes
app.get('/', (req, res) => {
    res.send("we are home");
});


//Connect to db
mongoose.connect(
    process.env.DB_CONNECTION, 
    () => console.log("connected to db")
    ); 

//Start Listening to server

app.listen(3000);

